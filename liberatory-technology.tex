
  \chapter{Liberatory technology}

  Before we discuss how to support ourselves developing liberatory
  software, we will provide an intuition for what we could produce,
  which would be more likely to be liberatory than not. To do this, we
  will analyse the model poised by \emph{social ecology} of a
  \emph{liberatory technology}. It may appear counter-intuitive for
  technology to have a central part in an ecological process, but
  studying how an ``appropriate'' technology may exist is crucial to
  forming an appropriate relation with the ecologies we reside in.

  One of the tenents of social ecology happily coincides with one of
  computing: if we can relieve ourselves of mundane, repetitive work,
  then we have the time and energy to reflect on what it is we are
  doing, and thus figure out what we must do. An evolutionary process
  must then lead to more and more self-aware and analytic actors, whom
  are better suited to the environment. Furthermore, they have to be
  able to adapt to new information and environments adequately, or
  they will fail should their original environment fail.

  This tenent is reflected in the concept of a liberatory technology. 
  The reader may or may not have an intuition for what a liberatory
  technology could be, so for the avoidance of doubt we will provide a
  definition. A liberatory technology is one which is thoroughly
  decentralised, can be adjusted and modified to fit its users'
  applications and environments, and is integrated in a way that it does
  not impede on how its users process things, instead complementing their
  processes. It is also designed to be understandable, usable and
  modifiable by a small community, allowing the community to support its
  use autonomously.
  
  \begin{samepage}
    \begin{quotation}
      We saw alternative technology as having great potential for
      decentralized, humanly scaled applications in the urban setting, and
      as lending itself to community control and directly democratic forms
      of decision making, thus providing a material base for the development
      of a decentralized, directly democratic society.

      \rightline{\cite{chodorkoff2010}}
    \end{quotation}
  \end{samepage}

  A liberatory technology would also facilitate its use for many purposes,
  and can self-regulate to reduce the duties of its operator. The users of
  such a technology could handle much larger projects, while still
  benefiting from the domain and local knowledge that they will have.
  
  \begin{quotation}
    The importance of machines with this kind of operational range can
    hardly be overestimated. They make it possible to produce a large
    variety of products in a single plant. A small or moderate-sized
    community using multi-purpose machines could satisfy many of its limited
    industrial needs without being burdened with underused industrial
    facilities. There would be less loss in scrapping tools and less need
    for single-purpose plants. The community's economy would be more compact
    and versatile, more rounded and self-contained, than anything we find
    in the communities of industrially advanced countries.

    \rightline{\cite{bookchin1971}}
  \end{quotation}

  This is fundamentally a book about programming, so the reader should
  know a computer is very much a multi-purpose machine, and a computer
  can self-regulate; one can compute almost anything that can be
  computed with a computer, and can do so faster than a human most of
  the time. Software on a computer can also protect itself if
  something goes wrong, and there are many techniques allowing
  software to continue after an exceptional situation. We can imagine
  that a computer would most likely appear in the processes a
  liberated community uses; but a notion of how to liberate the
  programmer and the act of programming itself needs to be developed.

  There are many ways to convince a computer to compute what you want
  it to, with varying qualities and peculiarities that convince people
  that some ways of conviction are usually more appropriate than
  others. We can reiterate the requirements common to social
  ecologists and radical programmers: it should be efficient, it
  should be understandable by another reader, the system should be
  modifiable by the user, and so on. Programmers often have problems
  fulfilling multiple of these requirements, by creating a slow
  program, or creating a fast but unreadable program, or producing an
  easily understood and performant program, which does not have any
  facilities for extension and modification, and so on. Fortunately,
  this situation is not innate to programming, and the qualities of a
  given programming environment can greatly affect the qualities of
  one's program. We believe that a \emph{dynamic environment}
  frequently provides the requisite facilities to pursue the best
  approach, and the situations and criteria in which one does not are
  gradually decreasing with time.

  \section{Dynamic environments}

  A \emph{dynamic environment} is roughly characterised by providing
  the programmer a way to have a conversation with the
  computer.\footnote{An analogy that was once punned on by Alan Kay:
    ``I had mentioned to someone that the \emph{prose} of then
    current programming languages was lower than a cocktail party
    conversation, and that great progress would have been made if we could
    even get to the level of making \emph{smalltalk}.''} While the
  conversation is not held in English or any other natural language to
  humans, it is more like a conversation than the interactions made with a
  \emph{static environment}, in which a programmer provides a
  \emph{compiler} program with source code, which then produces a
  program that the programmer can hardly modify after compilation. A
  programmer can ask many things, such as what the result of evaluating
  some code is, and what the properties of some object are. The computer
  may reply with questions, such as how to proceed when an error occurs.
  This conversational approach has several advantages:
  there is much less latency than when compiling, running and debugging
  entire programs, a programmer can see their code running and inspect
  the data it uses instead of having to imagine both, and a dynamic
  system can be trivially updated while it is still running,\footnote{
    Well, not exactly trivial: in some cases, the programmer has to be
    careful to not provide a running program functions that do not
    immediately work with types already in the system and vice versa.
    But this is far better than nothing; \emph{The long run} compares
    the qualities of this mutability with the immutable prior reasoning
    that is more common today.} which is crucial for programs that
  must run for a long time, such as servers and machine controllers.

  \begin{quotation}
    \begin{samepage}
      Traditionally, a program is thought of as describing objects
      to be created at run-time. \textelp{} But, being a description, the
      program cannot be directly used to visualize the objects in a
      running program; the programmer must make a visual leap.

      \rightline{\cite{ungar1995}}
    \end{samepage}

    \begin{samepage}
      When I looked at Java, I thought ``my goodness, how could they
      possibly \textelp{} survive all the changes, modifications, adaptations
      and interoperability requirements without a meta-system?'' Without
      even, for instance, being able to load new things in while you're
      running? So, the fact that people adopted this as some great hope,
      is probably the most distressing to me personally, as I said, since
      MS-DOS.

      \rightline{\cite{kay1997}}
    \end{samepage}
  \end{quotation}

  Note that designing and implementing a dynamic system may be more
  difficult than with a static system; for example, the representation
  of a global environment, which functions, variables, classes and so on
  reside in, may have to be made explicit, and data structures have to
  be made adaptable when class and type definitions change,\footnote{see
    \texttt{update-instance-for-redefined-class} in \emph{Common Lisp}}
  but those problems only have to be dealt with once by the
  implementation. The additional effort required to implement these
  mechanisms is quickly restored by the leverage a developer now has
  over the system, greatly broadening the range of programs an individual
  or a small community can maintain. This form of leverage provided by a
  liberatory technology pushes forward the
  ``limited horizons of achievement'' \parencite{mumford1964} which would
  otherwise bind a community to more mundane techniques; a user is
  capable of performing the same task with little effort, and with the
  same effort and time, the user can perform much larger tasks.

  This ability may also serve to adapt software to conditions and aims
  the original producers did not anticipate; avoiding the assumed
  planning that authoritarian technics\footnote{Note that
    Mumford uses the term \emph{democratic technics}, which is a precursor
    of sorts to the \emph{liberatory technology} of social ecology; and
    places an \emph{authoritarian technics} in contrast to it.} require,
  where ``one must not merely ask for nothing that the system does not
  provide, but likewise agree to take everything offered, \textelp{} in
  the precise [forms] that the system, rather than the person,
  requires.''  With the larger, but more specialised and categorised
  software environments we see today, adaptability has become an aim of
  some developers. It is, however, not feasible to implement or take
  advantage of, without a dynamic system to empower the users and
  developers to perform anything more than superficial or cosmetic
  changes to software.

  \section{Always has been \emph{malleable}}
  A concept of a \emph{malleable system} has surfaced recently, which
  like most things we find ourselves writing about, was quite promising
  at first. A malleable system appears oddly similar to a dynamic system,
  allowing for arbitrary composition of its components, but is more focused
  on having users perform their own composition.

  We found that a malleable system can be made of static components,
  and that some proponents of malleable systems assume static components
  in a malleable system. This assumption is a form of \emph{futurism}
  which ``[extends] the present into the future'' \parencite{bookchin1978},
  by establishing a hypothetical future where a malleable system can
  be produced, but somehow out of static components. It is scaling a
  structure which cannot be reasonably scaled, by vaguely suggesting how to
  adjust the relationship of its components; a kind of ``technique'' rightfully
  feared by programmers and social ecologists alike:

  \begin{quotation}
    Now, somebody could come along and look at a dog house and say,
    ``Wow! If we could just expand that by a factor of a hundred we could
    make ourselves a cathedral.'' It's about three feet high. That would
    give us something thirty stories high, and that would be really
    impressive. We could get a lot of people in there. \textelp{}
    [However,] when you blow something up [by] a factor of a hundred, it
    gets a factor of hundred weaker in its ability, and in fact, \textelp{}
    it would just collapse into a pile of rubble.
    
    \rightline{\cite{kay1997}}

    Futurism is the present as it exists today, projected, one hundred
    years from now. That's what futurism is. If you have a population of $ X $
    billions of people, how are you going to have food, how are you going
    to do this\ldots{}nothing has changed. All they do is they make everything
    either bigger, or they change the size -- you'll live in thirty story
    buildings, you'll live in sixty-story buildings. Frank Lloyd Wright
    was going to build an office building that was one mile high. That was
    futurism.
    
    \rightline{\cite{bookchin1978}}
  \end{quotation}

  A static malleable system attempts to scale a system that cannot be
  scaled; programs in static languages and static environments would be
  hopeless\footnote{There was an argument about if dynamic \emph{type
      systems} were any better at handling objects we don't know how to
    handle.  At the implementation level, most optimisations that can only
    be done in static systems are some forms of inlining, possibly
    \emph{monomorphizing} the generated code, so that it will be much more
    efficient with memory layouts it knows, at the cost of not being able
    to manipulate new types easily. At the language level, we would like
    for different protocols which ``do the same thing'' to cooperate, but
    this is not even possible in the \emph{structural type systems} that
    were advocated for by static type proponents, where types are sets of
    applicable functions. So, yes, static languages and their
    implementations are hopeless.}
  at using objects and types they have not been programmed to use (without
  mangling them, perhaps by serializing objects, which would violate the
  notion of both an object and a type).
  We have already argued that a static system is harder to develop
  with, but such a malleable system made of static components would have
  significant drawbacks that would limit its malleability.

  % TODO
  % we can probably move this explanation somewhere more relevant
  % I think the adaptor terminology is very useful to have
  % even if we decide to change it from adaptor to another word.
  Some forms of \emph{adaptors} would be used in a malleable system
  with static components; including a \emph{dynamic linker}, used to
  swap the static components that are in use within the system, and a
  \emph{embedded interpreter} for a dynamic language can be bundled
  with a static system to provide glue code between static
  components. The language is also typically used to provide the
  extension mechanism intended to be used by a user.
  One example of a successful malleable system, as mentioned in the
  Malleable System
  catalogue,\footnote{\url{https://malleable.systems/catalog/}} is ole
  \emph{GNU Emacs}, which we have happily used to type up this
  book. But working with its internals is not an enjoyable task; the
  \emph{XEmacs} developers knew this well, and attributed it to the
  non-abstract representation of many Emacs objects:

  \begin{quotation}
    \begin{samepage}
      We want our implementation of keymaps to be used: we want them to be
      an abstract data type, not something like ``if the third element of the
      alist is a cons whose car is a vector of length 7, then it represents
      an aliased indirection into the sixth element of the alist...''

      \rightline{\cite{zawinski2000}}
    \end{samepage}
  \end{quotation}

  How could this be a problem with a static malleable system? Emacs
  isn't entirely written in Emacs Lisp, and there is a C core that does
  a lot of important stuff for it. Interfaces between the two languages,
  one bytecode-interpreted dynamic language, and one compiled static
  language, are likely to be difficult to work with, and effectively
  lead a programmer to use strange representations that only make the
  interface easier to implement.

  The rest of implementing such an interface is also not easy:

  \begin{quotation}
    But basically, either you break the modularity so that you know
    what the module does with your objects and you have a maintenance
    nightmare, or you \emph{copy objects}, use \emph{smart pointers},
    or use \emph{reference counting} and you have a slow application.
    
    \rightline{Robert Strandh on \texttt{\#lisp}}
  \end{quotation}
  Implementing an interface between static and dynamic components
  doesn't appear like it gets easier -- never mind interfacing static
  components with other static components.

  Another extension of this issue is that developers cannot always
  foresee the ways in which their programs could be extended; the
  GNU Emacs developers did not expect a client to introduce new
  ``primitive''\footnote{In many Lisp systems, it is possible
    to define composite data types that are distinct from any in-built
    data types; \emph{Common Lisp} provides \texttt{defstruct} and
    \texttt{defclass} to do so. Emacs Lisp is very unique compared to
    other Lisp systems, and not just with data representation.}
  types, or they did not immediately see why that would be useful.

  As such, static systems cannot foster a complete ``read-write
  culture'' (as introduced in Chapter 2) which would be fundamental to
  malleable systems. In an attempt to facilitate this culture, the
  developers of a static system use adaptors, which are constrained to
  the capabilities of the components that provide their
  implementation. If a user needs to make changes to a static
  component that is used by the implementation of fundamental components
  of the system, they cannot use an adaptor to do so -- they must have
  the knowledge that was used to create the system in order to change
  it, and so a user could not use the embedded programming
  environment bundled in a static system to modify these
  components. They would effectively have to become a developer of the
  system, by retrieving the source code of the system, modifying it, and
  then rebuilding. This would violate read-write culture, as this is not
  something that can be expected of a user. We can also argue that
  this alone makes modification of static systems inherently
  inaccessible to those who need to program from another environment.

  %% TODO is there a way of making it clear that 8:25 is a timestamp
  %% for a video?
  Proponents of malleable suggest that relinking and subclassing
  can be used to provide extensions to a static system, however
  there are many extensions that would in fact require direct patching
  in order to work.
  An example of modification that the programmer did not imagine
  before appears around 8:25 into Dan Ingalls's demonstration of an old
  Smalltalk system \parencite{ingalls2017}, in which Ingalls modifies
  the way text selection is visualised, by replacing a method in the
  class that handles drawing the selection.
  If Ingalls was using a static system, subclassing this class and
  re-implementing the method there would not update programs
  itself; programs would also have to be updated to use this new
  class. While it is entirely possible to provide a better styling
  mechanism, allowing the user to describe how graphical objects should
  be rendered (as many desktop environments do today), the general
  solution appears long after the problem and a specific solution do,
  and the user can only wait for the developer to patch their components
  should they not be able to modify the components; Ingalls was able to
  replace the method almost immediately.

  A dynamic system makes it very possible for the user to inspect
  and modify the underlying system from any accessible environment.
  Additional problems that arise in making malleable systems simply do
  not exist in truly dynamic systems, as much of the host system can
  be reused in implementing the extension system. In many dynamic
  languages, it is possible to generate code at runtime, and introduce
  new bindings for functions, values, classes and so on, allowing a
  user to modify the program without the programmer having to
  specially allow them to. A domain specific language, should one be
  used,\footnote{Though we would be surprised if said language wasn't
    used already to write parts of the program!} can be compiled to
  the dynamic language, used interchangeably with components written
  directly in it, and use the provided, fast implementation of the
  language, as opposed to rolling an interpreter that is strictly
  slower than the host, and creating half a dynamic system to host the
  malleable system.

  An entirely dynamic system such as a Smalltalk or Lisp machine
  obviates even more problems, including that of communicating (with
  serialisation and deserialisation of) complex objects between processes.
  Future systems such as CLOSOS \parencite{strandh2013} will even
  eliminate sterilising objects to disk, as object storage will be
  persistent. As many issues with malleable systems are reduced with
  techniques performed by dynamic systems, we are well convinced a
  successful malleable system must be dynamic.

  \section{Who's doing the computation now?}

  Unfortunately, static systems still exist, and even more
  unfortunately, they may well be on the rise again. Dynamic
  optimisations and compilers have been applied to languages such as
  Lisp, APL, Smalltalk and Self, Java, Julia and so on for almost fifty
  years, and high level languages can frequently perform faster than static
  languages in symbolic processing.\footnote{One example is solving the
    n-queens problem in OCaml, a functional high level language, and in
    C++, an imperative low level language. The C++ program ran up to about
    $10\times$ slower than the OCaml program:
    \href{https://web.archive.org/web/20180216150158/http://flyingfrogblog.blogspot.co.uk/2011/01/boosts-sharedptr-up-to-10-slower-than.html}%
    {\nolinkurl{http://flyingfrogblog.blogspot.co.uk/2011/01/boosts-sharedptr-up-to-10-slower-than.html}}}
  But that is not a factor that could fit into the world view of a
  programmer fooled by \emph{Ousterhout's fallacy}, the false
  dichotomy between ``scripting'' languages (which are interpreted and
  have sub-optimal data representations) and ``system'' languages (which
  are compiled and have optimal data representations), who could not believe
  that a dynamic environment can be just as fast and provide guarantees
  like a static environment. Thus the solution must be to produce static
  systems with better guarantees, and work around problems (such as
  \emph{memory safety}) that simply do not exist in dynamic languages,
  and not having the benefits of late binding, introspection and so on.

  \hyphenation{Mas-och-ist}

  We had previously written an essay about static environments, entitled
  \emph{Masochist programming} \parencite{patton2019}. This odd description
  of a programmer that advocates for static environments relates to how
  they appear to want more work to do, and to have a much more
  ``painful'' experience programming. We wanted to know
  \textbf{what excuses does one have for bonding themselves to a static
    environment?} So you can write code for microcontrollers? Like that
  web server we heard of that falls over with 2,000 connections (which
  was called a denial of service to save face), which is clearly going
  to run on a microcontroller soon? To make micro-optimisations to drop
  levels of indirection that access to a compiler could do for you, and
  then optimise out some more? Is it more ``real time'' (whatever that
  means to you)?  Are you now free from watching your consing and
  control flow to produce consistent execution times? And you're designing
  for sub-millisecond latency? All of the above? Most people we ask
  are really doing none.

  While we're here: expressing concerns about the safety of C and C++
  programs has not and is not limited to proponents of other ``systems''
  languages,\footnote{We don't really think systems languages exist; as
    previously mentioned, machines that had operating systems written in
    Lisp and Smalltalk had some success, and were more advanced than
    Unix at the time; the Unix-Haters Handbook
    \parencite{garfinkel1994} provides some commentary from users of those
    machines who were forced to migrate to Unix systems.} contrary to
  popular belief. For example, the iconic Scheme textbook
  \emph{Structure and Interpretation of Computer Programs} makes note of
  the unsafe-by-default code C and C++ compilers generate:

  \begin{quotation}
    Compilers for popular languages, such as C and C++, put hardly any
    error-checking operations into running code, so as to make things run
    as fast as possible. As a result, it falls to programmers to
    explicitly provide error checking. Unfortunately, people often neglect
    to do this, even in critical applications where speed is not a
    constraint. Their programs lead fast and dangerous lives. For example,
    the notorious ``Worm'' that paralyzed the Internet in 1988 exploited
    the Unix operating system's failure to check whether the input buffer
    has overflowed in the \texttt{finger} daemon.

    \rightline{\cite{abelson1996}}
  \end{quotation}
  
  \hyphenation{po-sters}
  This ``radical'' desire of more toil reminds us of old Soviet
  propaganda posters; the kind where some proles are farming,
  metalworking, loading guns, that kind of thing, with some motivational
  caption that we can't read. In that time in Russia, industrialisation
  was highly sought out, in an ``age burdened by scarcity, when the
  achievement of socialism entailed \emph{sacrifices} and a
  \emph{transition period} to an economy of material abundance''
  \parencite{bookchin1971} and working to advance the industry as such
  was a noble thing to do with one's time. That was, of course, ninety
  or so years ago, and now with the possibility to greatly reduce toil,
  and capitalists already ordering production greater than what can be
  consumed (and then to burn off the surplus), it would be absurd to
  tell people to work more. Apparently not so for static programming
  environments!

  \section{The long run}
  But efficiency isn't the only desire of a static programmer; many
  proponents suggest that they are able to verify their behaviour at
  compile-time (summarised in the mantra ``if it compiles, it works'').
  Dynamic systems are usually designed to facilitate working with moving
  targets, so verification may not be desirable for many users, who could
  not produce a specification that could be verified before it is changed.

  \begin{quotation}
    Market pressure encourages the development and deployment of systems
    with large numbers of complex features. Often systems are deployed
    before the interaction between such features is well
    understood. During the lifetime of a system, the feature set will
    probably be changed and extended in many ways.
    
  \rightline{\cite{armstrong2003}}
  \end{quotation}
  
  Some systems even assume the opposite is inevitable, that there are
  errors in user programs, and attempt to behave correctly in the presence
  of software errors. Such systems may also have advantages for handling
  hardware (and other external) errors, as those are just another type of
  failure that the system can accommodate for. A dynamic approach is better
  suited for maintaining long-running processes that undergo changes, yet
  by the superficial appeal of being able to ``prove'' behaviour, and
  delivering the possibly false belief that one is safe from programming
  errors, it is mostly non-present in mainstream programming.
  
  (There are some cases where a dynamic environment simply couldn't work,
  such as deploying programs for constrained machines like microcontrollers
  and unmodifiable programs such as \emph{smart contracts}, but again
  the times an average programmer works with those are probably very
  overstated. However, many theorem prover programs today, such as Coq and
  ACL2, are very interactive, yet produce arguably the most static
  things programmers make. It would not be hard to assume that making
  testing environments more interactive would allow for greater
  confidence in the correctness of a project in less time.)
  
  \begin{quotation}
    But with C++ and Java, the dynamic thinking fostered by object-oriented
    languages was nearly fatally assaulted by the theology of static
    thinking inherited from our mathematical heritage and the assumptions
    built into our views of computing by Charles Babbage whose
    factory-building worldview was dominated by omniscience and omnipotence.

    And as a result we find that object-oriented languages have succumbed to
    static thinkers who worship perfect planning over runtime adaptability,
    early decisions over late ones, and the wisdom of compilers over the
    cleverness of failure detection and repair.

    \rightline{\cite{gabriel2002}}
  \end{quotation}

  The languages which may be considered dynamic or object-oriented today,
  like Python and Java, are in effect the worst of both worlds: unable to
  be adapted and migrated to new specifications like classic dynamic
  systems, and unable to be analysed and statically proven like static
  systems.

  The trend towards static monoliths with dubious claims to
  ``efficiency'' and ``correctness'' should be terrifying; prototyping
  and experimentation, which may become the means by which people
  adapt and integrate technics and our software into their contexts
  and use-cases, are going to become much harder by producing
  increasingly more static structures. The conversational and
  malleable aspects of dynamic systems make them almost always ideal
  in reducing toil for programmers, and a free society that requires
  programming certainly should educate and support its programmers in
  using such systems. Overstating the cases in which this is not the
  case is comparable to overstating the minimum toil a free society
  would need to support itself.
