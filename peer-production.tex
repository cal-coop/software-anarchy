
  \chapter{Peer production}

  Peer production could be described as ``coincidental'' organisation,
  where individuals organise to achieve a common goal, with no
  formalities and usually no other reasons to associate.
  This mode of production has appeared with the proliferation of the
  Internet, as it makes collaboration between strangers with common
  interests quite possible. It can be said that peer production
  maximises the agency of an individual producer, while promoting a free
  flow of information. Such a flow of information is arguably harder
  to prevent than allow in modern society, especially with programs and
  their source code; there are many websites that a developer can upload
  source code to, and be provided with issue and patch submission
  tracking at the very least. A hobbyist is very likely to release their
  source code by default when presented with these facilities.

  Infrastructure for hosting source materials is crucial to facilitating
  peer production, but it is not the only crucial component.
  \parencite{kleiner2010} goes very far into how to provide financial
  and material support for producers, proposing a
  \emph{venture commune} to decentrally manage materials. We would
  only reiterate the points and strategies presented in the manifesto, so
  we will not discuss it further in this book. Instead, we will discuss
  how to organise communities to engage in peer production, while
  preserving their decentral nature, without extinguishing but rather
  \emph{utilising} the plurality and dissensus of a decentral community.

  \section{Non-hierarchical people organisation}

  Static forms of organisation may hinder the creative and innovative
  processes of their participants. Binding ourselves to projects and
  individual manifestations of our line of thought is not sustainable,
  as we find ourselves frustrated with what we cannot change, and what
  we should have done, as we progress.
  
  \begin{quotation}
    \begin{samepage}
    If we use our creative power for ourselves, it destroys us. If we
    sacrifice ourselves in order to serve the creative power, then it
    creates us.

    \rightline{\cite{bennett2009}}
    \end{samepage}
  \end{quotation}

  It is also difficult to find resources and funding when one is
  investigating very new ideas, without a benefactor (such as a
  multi-billion dollar printer company, or a government or military
  contract) that trusts one to find something interesting, or has
  enough resources to throw at anyone.
  
  \begin{quotation}
    \begin{samepage}
      True innovation also involves questioning the assumptions that
      almost everyone agrees with. This can sometimes make those of
      us engaged in research feel a bit like thought criminals.
      \textelp{} Of course, no one is going to send inquisitors to our homes
      to persecute us for disagreeing with the mainstream. However, you
      can run out of funding very fast.
      
      \rightline{\cite{bracha2013}}
    \end{samepage}
  \end{quotation}
  
  By organising a group around the development of a project, the
  participants may deny themselves the potential of producing a new
  project that better supports their ideas. Peer production may
  allow its participants to diverge and follow their lines of thought
  more gracefully than a rigid collective or cooperative form; it is
  likely, should one find themselves traversing a new idea, that the
  people that one wants to collaborate with are outside a collective,
  and so at that point the collective is more or less reduced to
  an accounting service.

  \subsection{The Lisp ``Curse'' redemption arc}

  When a developer has introspective tools and some time to poke
  around, they are in a very good position to analyse difficult
  codebases, which are particularly large or are written in a way that is
  unnatural to the reader. However, the supposed secondary and
  tertiary effects of providing a programmer with sufficient power
  over their programming environment may be significant enough to deter
  cooperation according to an essay titled ``The Lisp Curse'',\footnote{
    \url{http://www.winestockwebdesign.com/Essays/Lisp_Curse.html}}
  which is frequently used as an excuse to avoid confrontation over social
  issues affecting the \emph{Common Lisp} community.\footnote{
    It has been
    suggested many times that one could substitute the name \emph{Lisp}
    for some other name, like \emph{JavaScript} or \emph{Python}, and
    most of the article wouldn't look too wrong.} The main points
  made are that technical issues in sufficiently powerful languages and
  environments become social issues, and that having such power reduces
  some natural cooperative force between programmers, causing them to
  part ways easily and thus not achieve anything significant without
  external discipline. This would spell disaster for our peer production
  model, if it weren't that the centralised models put in contrast to
  a dynamic-decentralised development model can only be worse at
  producing stagnation and removing agency from the user; which would
  greatly slow any experimentation and the progress of the community.
  In short, the apparent incoherence of peer production should be
  embraced instead of lamented, as we may stand to learn a lot from
  incomplete prototypes when trying to produce some sort of grand
  unified product.

  There are two apparent ``solutions'' that avoid this curse that we
  will explore. The first solution is to add the extension to the
  system via the implementation, forcing the community to adopt this
  extension, removing the agency of the user and setting them up to be
  screwed if the solution becomes a problem.  The second is to ensure
  that any task is too large to tackle without cooperation, by reducing
  the power and efficiency of each individual user, and in doing so,
  eliminating all facilities for the individual creative process.

  Neither solution is particularly appealing. If the provided extension
  has flaws that require it to be replaced, fixing the problem will
  affect many more clients, as opposed to if the client had more
  options. For example, \emph{JavaScript} used to use a \emph{callback}
  system (which is really a form of \emph{continuation-passing style})
  for interfacing with the outside world. Writing in a continuation-passing
  style manually was regarded by some as difficult to read and
  write,\footnote{Having too many callbacks in one function is often called
    \emph{callback hell}.} so a \emph{promise}-based system
  and some syntactic sugar (\texttt{async} and \texttt{await}) were
  introduced to make using it look like normal, synchronous
  code. Fortunately, promises are still compatible with
  continuation-passing style code, so it did not require any code to be
  replaced, but it still cuts a program into blocking and non-blocking
  parts.

  \begin{quotation}
    You still can't use them with exception handling or other control
    flow statements. You still can't call a function that returns a
    future from synchronous code.
    
    You've still divided your entire world into asynchronous and
    synchronous halves and all of the misery that entails. So, even if
    your language features promises or futures, its face looks an awful
    lot like the one on my strawman.

    \rightline{\cite{nystrom2015}}
  \end{quotation}

  Features such as asynchronous programming are very difficult to
  handle without getting it right the first time. The way to go forward
  while how to implement features is being debated is to provide a
  construct that subsumes it, such as providing access to implicit
  continuations like \emph{Scheme} or using another unique
  combination of syntax and constructs that provide something like
  continuations, such as \emph{monads} and the \texttt{do}-notation
  present in \emph{Haskell} and \emph{F\#}, allowing a programmer
  to wire the continuations into their asynchronous backend; or by
  providing many green threads, such as in \emph{Erlang}, and have the
  backend unblock the green threads when they are ready to continue. The
  latter two techniques facilitate composing various implementations, as
  they implement a common protocol which allows them to compose easily,
  where both implementations can be used in the same project if necessary.
  Thus, we are in a much safer position with \emph{more} power to
  reproduce special language constructs if they become problematic,
  and then to unify and compose multiple implementations, allowing
  participants to work without a consensus. \textcite{kay1997} succinctly
  states this view as ``the more the language can see its own
  structures, the more liberated you can be from the tyranny of a single
  implementation.''

  Disempowering an individual discourages experimentation,
  whether it be in attempting to implement a new concept, or modifying
  existing code to improve it. If the goal is to develop new concepts,
  then having many partial implementations is better than one complete
  implementation; as they reflect on different views of the concept. (An
  implementation is never really complete, and a concept is never really
  finished, anyway.) One complete implementation is prone to be wrong,
  and a linear progression of products provides less information to work
  with, compared to many experiments. If the goal is to produce stable
  software, knowledge of such experiments and prior work is very
  useful. Kay had frequently called various tenents of programming
  a ``pop culture'', where participants have no knowledge of their
  history. Without such experiments, we have no history to investigate,
  even in the near future. It would thus be ideal to experiment as much as
  possible, and use the lessons learnt to produce a comprehensive theory
  and then a comprehensive implementation; and such software may not
  require replacement as immediately as if it were developed in a
  centralised or linear manner.

  Disempowering a community has negative effects for creating one
  ``unified'' product, too. While inducing difficulty to go ahead with
  any decision that isn't unanimous leads to \emph{a} consensus, it is
  an entirely arbitrary consensus, which can be as terrible as it can be
  good. The resulting structure may be good at giving orders and
  techniques to its participants, but ``although everyone marches in
  step, the orders are usually wrong, especially when events begin to
  move rapidly and take unexpected turns.'' \parencite{bookchin1971}
  Suppose a sufficiently large group of scientists, say all the
  physicists in Europe, were all told to perform the same experiment
  with the same hypothesis, the administration in charge would be
  laughed at, as it would be hugely redundant and inefficient to
  investigate only one problem and one hypothesis with as many physicists.
  However, such a situation is presented as an ideal for
  software design, when groups pursuing their own hypotheses and theories
  are considered ``uncoordinated'', or called ``lone wolves''.\footnote{
    ``But what about producing a baseline implementation that is
    usually acceptable in any case?'' Such a library is only really
    determined after more analysis, and what constitutes the baseline is
    often what is required for the baseline for some other concept, which
    leads to an unpredictable situation.  As we will soon discuss, a
    baseline is usually decided upon anyway.}
  Disempowerment also precludes the group from attempting another
  strategy, without another unanimous decision on which to attempt.
  
  The most viable option is to go forward with multiple experiments,
  and provide participants with more power, so they may late bind and
  ignore the ``social problems'' produced by the diverse environment, in
  turn provided by having reasonable control over the environment.
  Measuring progress by the usage and completion of one implementation of
  a concept is an inherently useless measure; it would not consider the
  progress made in another implementation or design. Such a measure
  ``subjects people to itself'' \parencite{stirner1845} and inhibits their
  creative processes. ``You're imagining big things and painting for
  yourself \textelp{} a haunted realm to which you are called, an ideal
  that beckons to you. You have a fixed idea!''
  Progress on producing should be measured in how much of the
  \emph{design space} has (or can be easily) traversed, as opposed to
  the completion of one product; a poor design choice could entail a
  final product being unfit for its purpose, but a failed prototype is
  always useful for further designs to avoid. With that metric, a
  decentral development model is greatly surperior to a centralised
  model.

  \subsection{Community effects}
  
  Peer production can better support decentralised development that
  doesn't come upon a consensus for whatever reason. This is a natural
  effect of developing sufficiently broad concepts and concepts which
  the theory of is frequently changing; where there are many ways to
  implement the concept which are not always better or worse than
  one another. Examples of this theme are hash tables and pattern
  matching, for which there are many implementations with varying
  performance characteristics, and for which research appears to
  generate a new technique or optimisation every few years.
  
  Empirically, the Common Lisp community has not delivered on the
  prediction the author made, of many mutually incomprehensible
  incomplete implementations of any new concept. Pattern matching,
  again, was a concept that the author of the article believed would be
  implemented many times when functional programming becomes further
  put into the mainstream. Functional programming did indeed become
  fairly mainstream,\footnote{Of course, the reader -- pardon me --
    \textbf{Real Hackers} should be aware that functional techniques are
    not necessarily a replacement for object-oriented techniques, and thus
    can't be compared or ``moved on from'' to another.} and since then,
  there has been one pattern matching library widely used. In fact, the
  number of common pattern matching libraries has been reduced, with
  \emph{Optima} being deprecated in favour of the now common
  \emph{Trivia}, which uses a newer compilation technique. Furthermore,
  there have also only been one popular lazy evaluation library
  (\emph{CLAZY}), and one static type system implementation
  (\emph{Coalton}). So, empirically, there has not been an explosion
  of poor attempts at any functional programming libraries; or we have
  not heard of them.

  We may not have heard of any other of these genres of libraries
  prior to writing this book due to \emph{network effects}, which filter
  out many bad libraries when there are many options; with no
  disposition otherwise, one is most likely to follow the design choices
  that appear popular. For example, there are many \emph{testing
    frameworks} in Common Lisp (and it has become a joke to write another
  framework), but we can count most of the frameworks mentioned
  publicly per week on one hand.
  This has a sort of smoothing effect, where there are a small number
  of usually-good choices that are frequently recommended, greatly reducing
  the perceived entropy of the environment.\footnote{However, this can also
    lead to \emph{shared misery}, where users recommend what they have
    always used, even if they know there is a better alternative:
    \url{http://metamodular.com/Psychology/shared-misery.html} \\
    Again, when it is difficult to change one's decision, and one is
    now dependent on external factors, it may become very unfortunate that
    clustering occurs. For example, Fediverse server usage can be seen to
    follow \emph{Zipf's law}, where few servers support many users:
    \url{https://social.coop/@Stoori/100542845444542605}}

  A culture that encourages experimentation, but allows the community to
  settle on usually-good defaults, can remain cooperative and cohesive
  without risk of stagnation; the many forms of communication which do
  not require formal arrangements, and the rapid network effects
  produced by online communication can support both qualities. The
  community should also be aware of duplication of code when its
  prototypes converge, and make a goal of reducing such duplication and
  improving code quality; which is hard to ``sell'' as code quality is
  hard to quantify and concisely describe, but is of course necessary
  to support further development.

  \begin{quotation}
    Since execution performance is readily quantified, it is most often
    measured and optimized--even when increased performance is of
    marginal value \textelp{} Quality and reliability of software is much
    more difficult to measure\footnote{There are now some misleading
      metrics, such as the ``nine nines'' of uptime that an
      Erlang-based telephony system achieved with 14 machines, and the
      code quality \emph{badges} that frequently appear on open
      source project documentation. The former is a product of
      fault-tolerant hardware design as much as it is fault-tolerant
      software design, and the latter won't tell off the programmer for
      larger design problems, say, using a poor choice of data structures.}
    and is therefore rarely optimized. It is human nature to ``look for
    one's keys under the street-lamp, because that is where the light
    is brightest.''

    It should be a high priority among computer scientists to provide
    models and methods for quantifying the quality and reliability of
    software, so that these can be optimized by engineers, as well as
    performance.

    \rightline{\cite{baker1991}}
  \end{quotation}

  Beyond our commentary on development models, there are some other
  issues with the article that should be pointed out. The fatalism of the
  article is self-fulfilling, which is not helpful for attempting
  to rid Lisp of its ``curse''. If one takes the advice and avoids
  powerful languages and environments, then they will appear unpopular and
  its espousers can use it as evidence for their babbling, with a
  statement like ``Look, no one uses Lisp still! The Lisp Curse was
  right!'' Assuming this development model is a problem, is the aim to
  actually relieve the community of this issue, or is it to badmouth the
  community? It also supposes that the goals then, such as Lisp machines
  and operating systems, are the goals now. As suggested near the end of
  \emph{Always has been ``malleable''}, we do want a Lisp operating
  system, but the special machines are less necessary with clever
  compilation strategies today; but more importantly, this metric
  conveniently ignores that the choices in ``normal'' operating systems
  almost collapsed to Windows or some Unix, equally universally
  affecting other vendors, regardless of technology choices; instead
  opting to implicitly put the blame on Lisp users for the loss of
  diversity of operating systems. The foreword to
  \parencite{garfinkel1994} also names \emph{TOPS-20}, the
  \emph{Incompatible Time System} and \emph{Cedar} as victims of
  this collapse, which were not written in Lisp or a dynamic language,
  but were still disposed of around the same time.

  \textbf{In this sense, The Lisp ``Curse'' is only real because we make
    it real.} We set up metrics and constraints that promote
  conformist and centralist development strategies, then pat ourselves on
  the back for having made it impossible for anyone to test new ideas on
  their own. These sorts of metrics and organisational methods ``treat
  \textelp{} all twists and turns in the process of theorizing as more or
  less equivalently suspect, equivalently random flights of fancy,''
  \parencite{gillis2015} which have no real purpose. It would be interesting
  to see if consciously attempting to avoid this centralism would
  produce better quality software; allowing developers to go off on any
  interesting tangent, and breaking the illusion that there is one way
  to achieve the aim. This state should not pose any issues with
  sufficient communication; even if it does not appear very coordinated,
  or the group of diverging programmers appears uncooperative, they are
  much more likely to find the right approach and base for their
  product. The environment can also be made more condusive to finding
  new approaches, by opening communication channels, and publishing the
  \emph{source materials} required to implement a product.
  
  \section{Source materials}

  There is a concept of a \emph{read-write culture}, also known as
  \emph{remix culture}, where ``consumers'' of some product also
  modify and reproduce the product, making them also producers.
  \textcite{kleiner2010} provides a critique of the ``Creative
  Anti-Commons'', explaining that the \emph{Creative Commons} provide
  many options to enforce intellectual property and impede on derivative
  works, while its author suggests it would support such a culture.

  \begin{quotation}
    It is assumed that, as an author-producer, everything you make and
    everything you say is your property. The right of the consumer is
    not mentioned, nor is the distinction between producers and
    consumers of culture disputed.
  \end{quotation}

  Such a read-write culture is fundamentally impossible without
  sharing \emph{source materials}. These source materials may consist of
  source code, building instructions, original media, and so on. The
  transformations that one can make on many products without sources are
  very limited and of very low quality. For example, remixing a song is
  really only achievable by adding post-processing to the mastered
  audio, such as cutting the audio and adding effects, which is
  difficult to get right, and imprecise editing can confuse the
  listener. Modifying the behaviour a program with only its executable
  binary is basically impossible, and a consumer of those examples is
  likely to want to do both. To achieve that, the consumer would need to
  have the appropriate source materials.  The breadth of what can be
  required is illustrated in Figure~\ref{fig:production}.

  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{source-diagram}
    \caption{Many digital products are produced in complex ways, in
      which the source materials are almost impossible to recover.}
    \label{fig:production}
  \end{figure}

  \textbf{This read-write culture is also crucial for anarchist software}:
  our previously mentioned liberatory technology would only become stale
  and unusable, without constant adaptation and reciprocation of
  information regarding it. When our friend Jaidyn Levesque found that
  the Peer Production License did not have protections for preserving
  source materials, she modified the license to add them, producing a
  \emph{Cooperative Software License} that had source protections
  like the \emph{Affero General Public License}, while keeping the
  anti-capitalist stipulations in the Peer Production License; which
  may better facilitate a read-write culture.

  \emph{Ethical licenses}, such as the
  \emph{Non-Violent Public License} (based on the Cooperative Software
  License), potentially provide another option for developers who are
  concerned that their products may be used for very bad things, instead
  of living with the possibility, or avoiding developing what they
  want. These ambiguous situations come up more frequently than one
  might think; frequently when a product appears politically inert, but
  is used in a larger context, which can be very polarized. For example,
  a database may be used to track the finances and resources of a
  collective, or it may be used to comb through data collected from mass
  surveillance. A developer can now begin to distinguish between the
  uses they believe are moral, and the uses they believe are immoral, with
  such a license. While such licenses have always existed, recent
  observations of problems in free software culture have encouraged the
  creation of newer ethical and cooperative licenses; some of which are
  shown in Figure~\ref{fig:license-etymology}.

  \begin{figure}
    \centering
    \includegraphics[width=\textwidth]{license-etymology}
    \caption{A ``family tree'' of some licenses considered to be
      ethical and/or cooperative.}
    \label{fig:license-etymology}
  \end{figure}

  \hyphenation{thr-eat-en}
  An interesting question is, assuming that these licenses can be
  upheld in a court (which is not even clear for the \emph{GNU General
    Public License}, a relatively more permissive license), are there any
  actors that can violate them anyway?  It is unlikely states are going
  to be held properly accountable for violent acts they commit soon, and
  more unlikely that license violations will be part of hearings on such
  acts, but there is not much one could do to prevent \emph{some}
  actors from using your products other than to not produce them. A
  similar thing can be said for sufficiently large corporations which no
  court would want to try to control. Reasoning like this is often used
  to avoid these licenses, and we cannot deny that it is entirely
  possible that they could be worked around; but these licenses can
  still serve as deterrents to malicious adoption. For example, Google
  has banned use of any software licensed under the Affero General
  Public License,\footnote{``Code licensed under the GNU Affero General
    Public License (AGPL) MUST NOT be used at Google.''
    \url{https://opensource.google/docs/using/agpl-policy/}}
  as the ``virality'' of the license could require them to release
  sources for their internal code, which would threaten their
  proprietary surveillance model. This threat has been a crucial
  tactic for keeping copyleft code out of proprietary products for
  years, and we can adopt these tactics, but with a better
  sensibility for what software freedom may be.

  Another view on \emph{copyleft} licenses is that they may implicitly
  threaten state violence. For example, the \emph{Centre for a
    Stateless Society}, a publisher and think-tank for left-wing market
  anarchism, explicitly shuns both intellectual property and copyleft
  licensing.\footnote{``C4SS loathes intellectual property
    and finds copyleft licenses troubling in their implicit threat of
    state violence.'' \url{https://c4ss.org/about}} However,
  the alternative (of legally permitting such uses) may, again, lead to
  state and corporate violence. It may be difficult to avoid using the
  threat of the state to reduce the possible violent acts done; but
  the immediate threat of viral licensing deters many actors without
  having to do much oneself. (Using state violence to possibly prohibit
  itself is at least very amusing to some users of such licenses.)

  Ethical and cooperative licenses have had some success in the last
  five or so years, as they provide a very practical way to allow a
  programmer to effectively ``practise what they preach''.
  However, this idea was not understood by the authors of a so-called
  ``Anti-Capitalist Software License'' who appear to only care for the
  survival of a programming firm as anti-capitalism, and not any
  form of cooperation or collaboration, and put out statements such as:

  \begin{quotation}
    ACSL recognizes that the copyleft requirement of open source can
    be a drain on limited resources, can expose sensitive or secure
    information, and can put software at risk of theft.

    The availability of source code is less important than the
    organization of software labor.

    \rightline{\url{https://anticapitalist.software/}}
  \end{quotation}

  The former contradicts all three of basic anti-capitalist, peer
  production and software development theories: code itself should not
  be ``sensitive information'' that can be used to harm itself --
  \emph{security by obscurity} is widely acknowledged as a generally
  bad idea, and we should not need to elaborate on what may happen when
  making that assumption. Publishing and reusing free source code is
  an excellent way to get more resources,\footnote{This all began because
    someone realised open source code was a free replacement for labour
    for capitalists; suggesting that one has limited resources with open
    source code is to fully reject the premise one has started with!} and
  the existence of ``theft'' implies support of private property and
  that the author somehow did not consent to publishing their own source
  code! We are generally convinced the authors believe an
  anti-capitalist software firm is somehow a closed organisation that
  survives off hiding information from its consumers. Users of that
  license may be able to stay afloat by hiding source code and enforcing
  private property, but they have done nothing that can remotely be
  called anti-capitalist. The form of organisation suggested by such a
  license is nothing more than plain old capitalism with its
  intellectual property and information hoarding; we could draw a
  comparison to \emph{socialism for the rich} and call it
  \emph{socialism for the producer}, but it is not even that, as it
  paints collaboration between producers as a disadvantage! This attempt
  to avoid capitalism has somehow become so morbid, that it has wrapped
  around and reproduced capitalism again, and it did not even require a
  year or two for the Anti-Capitalist Software Party to turn in on its
  supporters; this license was truly dead on arrival.
